import logging
from gym.envs.registration import register

logger = logging.getLogger(__name__)

register(
    id='FishingDeterministic-v0',
    entry_point='gym_fishing.envs:FishingDeterministicEnv',
    timestep_limit=1000,
    nondeterministic = False,
)

