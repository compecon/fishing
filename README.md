# gym-fishing

Simple fishing environment.


# description 

Two discrete state variables x, p and one discrete action variable a

- x is the amount of fish in the sea
- p is the price of selling one unit of fish
- a is the amount to fish

reward r is given by p*a